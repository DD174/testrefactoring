<?php
use yii\helpers\Html;

/* @var $model \app\models\History */
/* @var $oldValue string */
/* @var $newValue string */
?>

<div class="bg-success ">
    <?php echo "$model->eventText " .
        "<span class='badge badge-pill badge-warning'>" . (Html::encode($oldValue) ?? "<i>not set</i>") . "</span>" .
        " &#8594; " .
        "<span class='badge badge-pill badge-success'>" . (Html::encode($newValue) ?? "<i>not set</i>") . "</span>";
    ?>

    <span><?= \app\widgets\DateTime\DateTime::widget(['dateTime' =>  $model->ins_ts]) ?></span>
</div>

<?php if (isset($model->user)): ?>
    <div class="bg-info"><?= $model->user->username; ?></div>
<?php endif; ?>
