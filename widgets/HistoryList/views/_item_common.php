<?php

use app\models\History;
use yii\helpers\Html;

/* @var $model History */
?>
<?php echo Html::tag('i', '', ['class' => 'icon icon-circle icon-main white ' . $model->getIconClass()]); ?>

<div class="bg-success ">
    <?= $model->getBody() ?>
</div>

<?php if ($username = $model->user->username ?? null): ?>
    <div class="bg-info"><?= $username; ?></div>
<?php endif; ?>

<?php if ($content = $model->getContent()): ?>
    <div class="bg-info">
        <?php echo $content ?>
    </div>
<?php endif; ?>

<div class="bg-warning">
    <?= $model->getFooter() ?? '' ?>
    <span><?= \app\widgets\DateTime\DateTime::widget(['dateTime' => $model->ins_ts]) ?></span>
</div>