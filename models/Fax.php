<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "fax".
 *
 * @property integer $id
 * @property string $ins_ts
 * @property integer $user_id
 * @property string $from
 * @property string $to
 * @property integer $status
 * @property integer $direction
 * @property integer $type
 * @property string $typeText
 *
 * @property User $user
 */
class Fax extends \yii\db\ActiveRecord implements \app\models\interfaces\HistoryObject
{
    const DIRECTION_INCOMING = 0;
    const DIRECTION_OUTGOING = 1;

    const TYPE_POA_ATC = 'poa_atc';
    const TYPE_REVOCATION_NOTICE = 'revocation_notice';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fax';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type'], 'required'],
            [['ins_ts'], 'safe'],
            [['user_id'], 'integer'],
            [['from', 'to'], 'string'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'ins_ts' => Yii::t('app', 'Created Time'),
            'user_id' => Yii::t('app', 'User ID'),
            'from' => Yii::t('app', 'From'),
            'to' => Yii::t('app', 'To')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * @return array
     */
    public static function getTypeTexts()
    {
        return [
            self::TYPE_POA_ATC => Yii::t('app', 'POA/ATC'),
            self::TYPE_REVOCATION_NOTICE => Yii::t('app', 'Revocation'),
        ];
    }

    /**
     * @return mixed|string
     */
    public function getTypeText()
    {
        return self::getTypeTexts()[$this->type] ?? $this->type;
    }

    /**
     * @inheritDoc
     */
    public function body(): ?string
    {
        if (isset($this->document)) {
            return \yii\helpers\Html::a(
                Yii::t('app', 'view document'),
                $this->document->getViewUrl(),
                [
                    'target' => '_blank',
                    'data-pjax' => 0
                ]
            );
        }

        return '<i>No document</i>';
    }

    /**
     * @inheritDoc
     */
    public function iconClass(): string
    {
        return 'fa-fax bg-green';
    }

    /**
     * @inheritDoc
     */
    public function footer(): ?string
    {
        return Yii::t(
            'app',
            '{type} was sent to {group}',
            [
                'type' => $this->getTypeText(),
                'group' => isset($this->creditorGroup) ? \yii\helpers\Html::a(
                    $this->creditorGroup->name,
                    ['creditors/groups'],
                    ['data-pjax' => 0]
                ) : ''
            ]
        );
    }

    /**
     * @inheritDoc
     */
    public function content(): ?string
    {
        return null;
    }
}
