<?php


namespace app\models\interfaces;

/**
 * Для корректного отображения объектов в истории они должны реализовывать этот интерфейс
 *
 * @package app\models\interfaces
 */
interface HistoryObject
{
    /**
     * Человоко-понятное описание деталий объекта в истории
     * @return string|null
     */
    public function body(): ?string;

    /**
     * css класс иконки отображаемой у объекта в истории
     * @return string|null
     */
    public function iconClass(): ?string;

    /**
     * детали объекта
     * @return string|null
     */
    public function footer(): ?string;

    /**
     * комментарий :)
     * @return string|null
     */
    public function content(): ?string;
}